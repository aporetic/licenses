# README

## MIT

Use for any of the following type of projects:
* standards, e.g., a protocol
* library which will require integration into proprietary services

## GPL

Use for:
* open source projects only meant to be used by other open source projects.

[more info](https://choosealicense.com)
